  function findVideos() {
      let videos = document.querySelectorAll('.video');

      //обрабатываем все найденные блоки с видео, в цикле через функцию setupVideo
      for (let i = 0; i &lt; videos.length; i++) {
          setupVideo(videos[i]);
      }
  }


  function setupVideo(video) {
      let link = video.querySelector('.video__link');
      let media = video.querySelector('.video__media');
      let button = video.querySelector('.video__button');

      //получаем id из атрибута с помощью функции
      let id = parseMediaURL(media);

      //добавляем событие обработки клика на нашем блоке с фоновым изображением
      video.addEventListener('click', () => {

          //создаём фрейм с помощью функции и полученного id
          let iframe = createIframe(id);

//        удаляем ссылку
          link.remove();
//          удаляем кнопку
          button.remove();
//      и загружаем фрейм с видео в шаблон
          video.appendChild(iframe);
      });

      link.removeAttribute('href');
      video.classList.add('video--enabled');
  }

  // получение ID видео из нашего атрибута id-url
  function parseMediaURL(media) {
      let match = media.getAttribute('id-url');
      return match;
  }

  function createIframe(id) {
      let iframe = document.createElement('iframe');

      iframe.setAttribute('allowfullscreen', '');
      iframe.setAttribute('allow', 'autoplay');
      iframe.setAttribute('src', generateURL(id));
      iframe.classList.add('video__media');

      return iframe;
  }

  function generateURL(id) {
      let query = '?rel=0&showinfo=0&autoplay=1';

      return 'https://www.youtube.com/embed/' + id + query;
  }

  //фактически вызываем пуск основного цикла этого скрипта
  findVideos();
